#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // styling for the labels for the player names
    ui->player1Label->setFont(QFont("Liberation Serif", 14, QFont::Normal));
    ui->player1Label->setStyleSheet("QLabel{color: blue;}");
    ui->player2Label->setFont(QFont("Liberation Serif", 14, QFont::Normal));
    ui->player2Label->setStyleSheet("QLabel{color: red;}");
    // make sure that the name of the current player is bold
    connect(ui->tictactoe, SIGNAL(changePlayer()), this, SLOT(boldCurrentPlayerName()));
    // bold the name of the first player to play at the start
    boldCurrentPlayerName();
    // enabling of the actions
    connect(ui->actionNewGame, SIGNAL(triggered()), this, SLOT(startNewGame()));
    connect(ui->actionQuitGame, SIGNAL(triggered()), qApp, SLOT(quit()));
    // initialise the configuration
    configuration = GameConfiguration::getInstance();
}

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * a function which manages the start of a new game
 * @brief MainWindow::startNewGame
 */
void MainWindow::startNewGame()
{
    // reset the player names
    ui->player1Label->setText("");
    ui->player2Label->setText("");
    // reset the game side
    configuration->setGameSide(SideRange::minRange);
    // clear configuration fields
    configuration->setPlayer1Name("");
    configuration->setPlayer2Name("");

    // if the user presses "cancel", the new game is aborted
    if(configuration->exec() == QDialog::Rejected){
        // abortion
        return;
    }
    // configuration of player names
    ui->player1Label->setText(configuration->getPlayer1Name());
    ui->player2Label->setText(configuration->getPlayer2Name());
    int gameSide = configuration->getGameSide();
    // Mode mode = configuration->getMode();
    // adjustment of the tictactoe board size
    ui->tictactoe->setFixedHeight(50 * (gameSide + 1));
    ui->tictactoe->setFixedWidth(50 * (gameSide + 1));
    // hide the game title
    ui->gameTitle->setVisible(false);
    // update the side of the game
    ui->tictactoe->setGameSide(gameSide);
    // launch the game
    ui->tictactoe->startOrRestartGame();
    // enable the tictactoe board
    ui->tictactoe->setEnabled(true);
}

/**
 * A function which bolds the name of the current player
 * @brief MainWindow::boldCurrentPlayerName
 */
void MainWindow::boldCurrentPlayerName()
{
    // bold the label of player 1 if it is player1's turn
    QFont playerFont = ui->player1Label->font();
    playerFont.setBold(ui->tictactoe->getCurrentPlayer() == Player::Player1);
    ui->player1Label->setFont(playerFont);

    // bold the label of player 2 if it is player2's turn
    playerFont.setBold(ui->tictactoe->getCurrentPlayer() == Player::Player2);
    ui->player2Label->setFont(playerFont);
}

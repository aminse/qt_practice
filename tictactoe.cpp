#include "tictactoe.h"
#include <QDebug>

TicTacToe::TicTacToe(QWidget *parent)
    : QWidget(parent)
{
    player = Player::Player1;
    winner = Winner::NoWinnerYet;
    // board creation
    createBoard();
}

TicTacToe::~TicTacToe()
{
}

/**
 * @brief TicTacToe::resetBoard
 * a function which resets the board
 */
void TicTacToe::resetBoard()
{
    for(int i = 0; i < MetaData::ROWSCOLUMNS * MetaData::ROWSCOLUMNS; ++i){
        board.at(i)->setText(MetaData::spaceCharacter);
    }
}

/**
 * @brief TicTacToe::setCurrentPlayer
 * @param p
 * the function sets a new value for the player
 */
void TicTacToe::setCurrentPlayer(Player p)
{
    player = p;
}

/**
 * @brief TicTacToe::getPlayer
 * @return
 * A function which returns the player
 */
Player TicTacToe::getPlayer() const
{
    return player;
}

/**
 * @brief TicTacToe::handleClicksOnBoard
 * a slot to handle clicks on the board
 */
void TicTacToe::handleClicksOnBoard(int buttonIndex)
{
    // case of an invalid index (0 < valid index < size of the board)
    if(buttonIndex < 0 || buttonIndex >= board.size()) return;

    // the button which was clicked on
    QPushButton* button = board.at(buttonIndex);
    // styling of the button
    button->setFont(QFont("Tahoma", 12, QFont::Bold));

    // make sure the button which was clicked on does not contain text
    if(button->text() != MetaData::spaceCharacter) return;

    QString symbol;

    // write text on the button accordingly
    if(player == Player::Player1){
        symbol = MetaData::player1Symbol;
        button->setText(symbol);
        button->setStyleSheet(QString("QPushButton{color: ") + MetaData::player1Colour + "; background: lightyellow;}");
        button->setDisabled(true);
    }
    else if(player == Player::Player2){
        symbol = MetaData::player2Symbol;
        button->setText(symbol);
        button->setStyleSheet(QString("QPushButton{color: ") + MetaData::player2Colour + "; background: lightgreen;}");
        button->setDisabled(true);
    }
    // determine the winner
    winner = determineWinner(symbol, buttonIndex);
    if(winner == Winner::NoWinnerYet){
        if(player == Player::Player1){
            setCurrentPlayer(Player::Player2);
        }else if(player == Player::Player2){
            setCurrentPlayer(Player::Player1);
        }
    }
    else{
        // disabling of the board
        this->setDisabled(true);
        QTimer::singleShot(MetaData::freezeTime, this, SIGNAL(finishGame()));
        connect(this, SIGNAL(finishGame()), this, SLOT(handleEndOfGame()));
    }
}

/**
 * @brief TicTacToe::handleEndOfGame
 * A function to handle the end of the game
 */
void TicTacToe::handleEndOfGame()
{
    // emptying of the tictactoe window
    // retrieve layout
    QLayout* layout = this->layout();
    // placeholder layout item
    QLayoutItem* layoutItem;
    // retrieve the layout items, delete their widget and then
    // delete the layout items
    // 0 1 2 3 4
    // 1 2 3 4 => 0 1 2 3
    // 1 2 3 => 0 1 2
    while(layout != nullptr && (layoutItem = layout->takeAt(0)) != nullptr){
        // delete the widget of the layout item
        delete layoutItem->widget();
        // delete the layout item
        delete layoutItem;
    }
    // delete the layout
    delete layout;
    // clear the board
    board.clear();

    // creation of the layout for the window to display the outcome of the game
    QVBoxLayout* vLayout = new QVBoxLayout(this);
    vLayout->setAlignment(Qt::AlignCenter);
    // label and restart button
    QLabel* restartLabel = new QLabel(this);
    QPushButton* restartButton = new QPushButton("restart", this);
    QString restartLabelColour;
    QString restartButtonColour;
    if(winner == Winner::player1){
        restartLabelColour = QString("QLabel{color: ") + MetaData::player1Colour + ";}";
        restartButtonColour = QString("QPushButton{color: ") + MetaData::player1Colour + ";}";
    }
    else if(winner == Winner::player2){
        restartLabelColour = QString("QLabel{color: ") + MetaData::player2Colour + ";}";
        restartButtonColour = QString("QPushButton{color: ") + MetaData::player2Colour + ";}";
    }
    else if(winner == Winner::Draw){
        restartLabelColour = QString("QLabel{color: ") + MetaData::drawColour + ";}";
        restartButtonColour = QString("QPushButton{color: ") + MetaData::drawColour + ";}";
    }

    // style the button
    restartButton->setMinimumHeight(40);
    restartButton->setMinimumWidth(100);
    restartButton->setFont(QFont("Liberation Serif", 14, QFont::Bold));
    restartButton->setStyleSheet(restartButtonColour);
    // styling of the label
    restartLabel->setFont(QFont("Liberation Serif", 14, QFont::Bold));
    restartLabel->setStyleSheet(restartLabelColour);
    restartLabel->setText("Temporary Text");
    // organize the widgets in the layout
    vLayout->addWidget(restartLabel);
    vLayout->addWidget(restartButton);
    // enable the titctactoe window
    this->setEnabled(true);
    connect(restartButton, SIGNAL(clicked()), this, SLOT(restartGame()));
}

/**
 * @brief TicTacToe::createBoard
 * a function which creates the board of the tictactoe game
 */
void TicTacToe::createBoard()
{
    // creation of the grid object
    QGridLayout* grid = new QGridLayout(this);
    // creation of a QSignalMapper object
    QSignalMapper* mapper = new QSignalMapper(this);
    for(int row = 0; row < MetaData::ROWSCOLUMNS; ++row){
        for(int column = 0; column < MetaData::ROWSCOLUMNS; ++column){
            // create a button
            QPushButton* button = new QPushButton(this);
            // set up the button: text and dimensions
            button->setText(MetaData::spaceCharacter);
            button->setMinimumHeight(50); // pixels
            button->setMinimumWidth(50);
            // add the button to the grid
            grid->addWidget(button, row, column);
            // map every button with their board index
            mapper->setMapping(button, board.size());
            // signal and slots
            // the connection of a signal to a slot of function
            // essentially, a signal represents the occurrence
            // of an event. and the slot is the function which
            // gets executed as a result of the occurrence of the
            // event.
            /** connection between a signal and a slot
             * connect(sender, signal, receiver, slot);
             * connect(button, SIGNAL(clicked()), this, SLOT(thisistheslot()));
             * sender and receiver have to be derived from
             * the QObject class, and the sender must include
             * the Q_OBJECT macro in a private section of its
             * declaration.
             */
            // connection of the click on a button to a slot of the QSignalMapper class
            // which is going to emit a mappedInt signal as a result
            connect(button, SIGNAL(clicked()), mapper, SLOT(map()));
            connect(mapper, SIGNAL(mappedInt(int)), this, SLOT(handleClicksOnBoard(int)));
            // add the button to the board
            board.append(button);
        }
    }
}

/**
 * A function which determines the winner of the game
 * @brief TicTacToe::determineWinner
 * @return
 */
Winner TicTacToe::determineWinner(const QString& symbol, int butttonIndex)
{
    // step 1: get the row number and column number of the clicked
    // button in the grid
    int rowNumber = butttonIndex / MetaData::ROWSCOLUMNS;
    int columnNumber = butttonIndex % MetaData::ROWSCOLUMNS;

    // counting variable
    int counter = 0;

    // horizontal check
    // forward check
    int newColumn = columnNumber;
    bool validateSecondCheck = true;
    while(++newColumn < MetaData::ROWSCOLUMNS){
        // position of next button in the board
        int newPosition = rowNumber * MetaData::ROWSCOLUMNS + newColumn;
        // retrieve next button
        QPushButton* button = board.at(newPosition);
        // check if the next button does not have the desired symbol
        if(button->text() != symbol){
            validateSecondCheck = false;
            break;
        }
        else{
            // count the symbol on next button
            ++counter;
        }
    }
    // horizontal check: backward check
    newColumn = columnNumber;
    while(validateSecondCheck && --newColumn >= 0){
        // position of hte next button in the board
        int newPosition = rowNumber * MetaData::ROWSCOLUMNS + newColumn;
        // retrieve the next button
        QPushButton* button = board.at(newPosition);
        // check if the next button does not have the desired symbol
        if(button->text() != symbol){
            break;
        }
        else{
            // count the symbold the next button
            ++counter;
        }
    }
    // did the player win horizontal?
    if(++counter == MetaData::ROWSCOLUMNS){
        if(symbol == MetaData::player1Symbol){
            return Winner::player1;
        }
        else if(symbol == MetaData::player2Symbol){
            return Winner::player2;
        }
    }

    // vertical check: upward and backward
    counter = 0;
    validateSecondCheck = true;
    int newRow = rowNumber;
    // upward check
    while(--newRow >= 0){
        // get the position index of the next position in the upward direction
        int newPositionIndex = newRow * MetaData::ROWSCOLUMNS + columnNumber;
        // retrieve the button on which the player made his move
        QPushButton* button = board.at(newPositionIndex);
        if(button->text() != symbol){
            validateSecondCheck = false;
            break;
        }else{
            ++counter;
        }
    }

    // downward check
    newRow = rowNumber;
    while(validateSecondCheck &&  ++newRow < MetaData::ROWSCOLUMNS){
        // get the position index of the next position the downward direction
        int newPositionIndex = newRow * MetaData::ROWSCOLUMNS + columnNumber;
        // retrieve the button on which the player made his move
        QPushButton* button = board.at(newPositionIndex);
        if(button->text() != symbol){
            break;
        }else{
            ++counter;
        }
    }

    // did the player win vertically?
    if(++counter == MetaData::ROWSCOLUMNS){
        if(symbol == MetaData::player1Symbol){
            return Winner::player1;
        }
        else if(symbol == MetaData::player2Symbol){
            return Winner::player2;
        }
    }

    // backslash diagonal check
    // upward direction
    counter = 0;
    validateSecondCheck = true;
    // row and column number for the next position in the diagonal
    newRow = rowNumber;
    newColumn = columnNumber;
    while(--newRow >= 0 && --newColumn >= 0){
        // get the position index of the next position
        int newPositionIndex = newRow * MetaData::ROWSCOLUMNS + newColumn;
        // retrieve the button at the new position
        QPushButton* button = board.at(newPositionIndex);
        if(button->text() != symbol){
            validateSecondCheck = false;
            break;
        }else{
            ++counter;
        }
    }
    // downward check
    newRow = rowNumber;
    newColumn = rowNumber;
    while(validateSecondCheck && ++newRow < MetaData::ROWSCOLUMNS && ++newColumn < MetaData::ROWSCOLUMNS){
        // get the position index of the next position in the diagonal
        int newPositionIndex = newRow * MetaData::ROWSCOLUMNS + newColumn;
        // retrieve the button at the new position
        QPushButton* button = board.at(newPositionIndex);
        if(button->text() != symbol){
            break;
        }else{
            ++counter;
        }
    }
    // did the player win diagonally? ( backslash direction)
    if(++counter == MetaData::ROWSCOLUMNS){
        if(symbol == MetaData::player1Symbol){
            return Winner::player1;
        }else if(symbol == MetaData::player2Symbol){
            return Winner::player2;
        }
    }

    // forward slash diagonal check
    // upward direction
    counter = 0;
    validateSecondCheck = true;
    newRow = rowNumber;
    newColumn = columnNumber;
    while(--newRow >= 0 && ++newColumn < MetaData::ROWSCOLUMNS){
        // index position of the next position
        int newPositionIndex = newRow * MetaData::ROWSCOLUMNS + newColumn;
        // retrieve the button at the next position
        QPushButton* button = board.at(newPositionIndex);
        if(button->text() != symbol){
            validateSecondCheck = false;
            break; // stop the upward check
        }else{
            ++counter;
        }
    }
    // downward direction
    newRow = rowNumber;
    newColumn = columnNumber;
    while(validateSecondCheck && ++newRow < MetaData::ROWSCOLUMNS && --newColumn >= 0){
        // index position of the next position
        int newPositionIndex = newRow * MetaData::ROWSCOLUMNS + newColumn;
        // retrieve the button at the next position
        QPushButton* button = board.at(newPositionIndex);
        if(button->text() != symbol){
            break;
        }else{
            ++counter;
        }
    }
    // did the player win diagonally? (forward slash)
    if(++counter == MetaData::ROWSCOLUMNS)
    {
        if(symbol == MetaData::player1Symbol){
            return Winner::player1;
        }
        else if(symbol == MetaData::player2Symbol){
            return Winner::player2;
        }
    }

    // check for the draw
    for(int i = 0; i < MetaData::BOARDSIZE; ++i){
        if(board.at(i)->text() == MetaData::spaceCharacter){
            return Winner::NoWinnerYet;
        }
    }
    return Winner::Draw;
}

/**
 * A function to manage the restart of the game
 * @brief TicTacToe::restartGame
 */
void TicTacToe::restartGame()
{
    // set the first player to start playing
    player = Player::Player1;

    // empty the tictactoe widget if necessary
    QLayout* layout = this->layout();
    QLayoutItem* layoutItem;
    while(layout != nullptr && (layoutItem = layout->takeAt(0)) != nullptr){
        delete layoutItem->widget();
        delete layoutItem;
    }
    delete layout;
    board.clear();

    // create a new board
    createBoard();
    this->setEnabled(true);
}

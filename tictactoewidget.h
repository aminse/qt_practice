#ifndef TICTACTOEWIDGET_H
#define TICTACTOEWIDGET_H

#include <QWidget>
#include <QList>
#include <QPushButton>
#include <QGridLayout>
#include <QSignalMapper>
#include <QTimer>
#include <QLabel>

struct MetaData{
    // static constexpr int ROWSCOLUMNS = 4;
    // static constexpr int BOARDSIZE = ROWSCOLUMNS * ROWSCOLUMNS;
    static constexpr const char* spaceCharacter = " ";
    static constexpr const char* player1Symbol = "X";
    static constexpr const char* player2Symbol = "O";
    static constexpr const char* player1Colour = "blue";
    static constexpr const char* player2Colour = "red";
    static constexpr const char* drawColour = "purple";
    static constexpr int freezeTime = 2000; // 2 seconds
};

// an enum to represent the players
enum Player{
    Player1, Player2
};

// a enum to represent the winner
enum Winner{
    player1, player2, Draw, NoWinnerYet
};

class TicTacToeWidget : public QWidget
{
    Q_OBJECT

public:
    TicTacToeWidget(QWidget *parent = nullptr);
    ~TicTacToeWidget();
    // a function which resets the board
    void resetBoard();
    // setter for the player
    void setCurrentPlayer(Player p);
    // getter for the player
    Player getCurrentPlayer()const;
    // a function to set the side of the game
    void setGameSide(int);

signals:
    // a signal to signal the end of the game session
    void finishGame();
    // a signal to signal that it is the turn of another player
    void changePlayer();
private slots:
    // a slot to handle clicks on the board
    void handleClicksOnBoard(int);
    // a slot tot handle the end of the game
    void handleEndOfGame();
public slots:
    // a function to manage the restart of the game
    void startOrRestartGame();
private:
    // a function which creates the board
    void createBoard();

    // a function which determines the winner of the game
    Winner determineWinner(const QString&, int);

private:
    // the game board
    QList<QPushButton*> board;
    // an object to represent the players
    Player player;
    // an object to represent the outcome of the game
    Winner winner;
    // a variable to store the side of the tictactoe game
    int gameSide;
};
#endif // TICTACTOEWIDGET_H

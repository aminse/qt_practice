#ifndef TICTACTOE_H
#define TICTACTOE_H

#include <QWidget>
#include <QList>
#include <QPushButton>
#include <QGridLayout>
#include <QSignalMapper>
#include <QTimer>
#include <QLabel>

struct MetaData{
    static constexpr int ROWSCOLUMNS = 4;
    static constexpr int BOARDSIZE = ROWSCOLUMNS * ROWSCOLUMNS;
    static constexpr const char* spaceCharacter = " ";
    static constexpr const char* player1Symbol = "X";
    static constexpr const char* player2Symbol = "O";
    static constexpr const char* player1Colour = "blue";
    static constexpr const char* player2Colour = "red";
    static constexpr const char* drawColour = "purple";
    static constexpr int freezeTime = 2000; // 2 seconds
};

// an enum to represent the players
enum Player{
    Player1, Player2
};

// a enum to represent the winner
enum Winner{
    player1, player2, Draw, NoWinnerYet
};

class TICTACTOE : public QWidget
{
    Q_OBJECT

public:
    TICTACTOE(QWidget *parent = nullptr);
    ~TICTACTOE();
    // a function which resets the board
    void resetBoard();
    // setter for the player
    void setCurrentPlayer(Player p);
    // getter for the player
    Player getPlayer()const;

signals:
    void finishGame();
private slots:
    // a slot to handle clicks on the board
    void handleClicksOnBoard(int);
    // a slot tot handle the end of the game
    void handleEndOfGame();
    // a function to manage the restart of the game
    void restartGame();
private:
    // a function which creates the board
    void createBoard();

    // a function which determines the winner of the game
    Winner determineWinner(const QString&, int);

private:
    // the game board
    QList<QPushButton*> board;
    // an object to represent the players
    Player player;
    // an object to represent the outcome of the game
    Winner winner;
};
#endif // TICTACTOE_H
